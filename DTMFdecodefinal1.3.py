import RPi.GPIO as GPIO
import sys
import time
import os

GPIO.setmode(GPIO.BOARD)
GPIO.setup(29, GPIO.IN, pull_up_down=GPIO.PUD_DOWN) #FLAG PIN
GPIO.setup(31, GPIO.IN, pull_up_down=GPIO.PUD_DOWN) #binary input pins
GPIO.setup(33, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
GPIO.setup(35, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
GPIO.setup(37, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)

debounce = 0.05 # debounce for dtmf keying
star = [1,0,1,1] #set star as binary 11
rawcode = [0,0,0,0] #initialize empty lists
sentcode = []

print ('starting')
while True:
    for i in range (0,4):
        print ('awaiting next input')
        GPIO.wait_for_edge(29, GPIO.RISING)
        print ('pin 29 rising edge')

        #'gpio.input(num)' returns 1 or 0 for hi/low	
        rawcode = [GPIO.input(31), GPIO.input(33), GPIO.input(35), GPIO.input(37)]

        #place each successive list of binary inputs in a list of lists (for the whole 4 number code)	
        sentcode.insert(i,rawcode)
        print(sentcode)
        print(rawcode)
        time.sleep(debounce) # short delay to prevent one intermittent input being read as one
        if (star in sentcode) and i != 3: # if star is in wrong place, reject everything and start over
            rawcode = [0,0,0,0]
            sentcode = []
            print ('recieved star early, rejecting code')
        #break out of and restart for loop	
            break

    print ('sentcode is', sentcode)
    #start converting binary lists to integers
    #do not need to convert last item of list, since we know it's star
    actionlist = []  #clear this list from last use- final list to determine what to do
    while len(sentcode) == 4:
        if sentcode[3] == star:
            print ('star found, processing list')
            for v in range(0,3):
                print ('v in range is',v)
                parselist = sentcode[v]
                print ('parselist',v,'is',parselist)
                parselist.reverse()
                print ('parselist has been reversed, is now', parselist)
                b = 0
                for i in range(0,len(parselist)):
                    if parselist[i]:
                        b = b + 2**i
                    print (b,'is b')
                actionlist.append(b)
                if v == 2:
                    sentcode = []
        else:
            sentcode = []   
            print (actionlist)

    if actionlist == [1,2,3]:                     #diagnostic
        print('Test code received, all is well.')
    elif actionlist == [8,8,8]: #send system status, and check that repeating numbers work
        print('system online and ready')
    elif actionlist == [1,4,7]:
        print('do a thing')
    elif actionlist == [2,5,8]:
        print('do something else')
    else:
        print('code not found. resetting')

