## Raspberry Pi DTMF decoder board

Hardware decoding of DTMF tones with a raspberry pi. Works with any radio, as it only needs an audio input. An MT8870 decodes the tones and a SN74LVC245 shifts the voltage down to 3.3v. There are a lot of options for digital radio modes and modules today, but for a HAM operator this seemed like a very versatile system as nearly every radio can produce DTMF tones from it's keypad; this means nothing special is needed for sending or decoding a digital signal, and works with equipment you already have. I myself have it hooked up to an old VHF handheld as the receiver.
![schematic](dtmfschematic.png)

### Program usage
The python program fills a 4 index list of DTMF codes recieved from the audio output of any radio. It expects three DTMF codes (0-9, a,b,c,d, or #) followed by a "\*" key. This key acts as the 'enter' button for your 3 digit code as well as clears the list in the event a number was missed (due to interference, miss-pressing a button, etc.). Once the program has received a valid code that ends with a star (i.e [3,2,1,\*] ), it can act on it, in whatever way you choose.  

### Quick code walkthrough
DTMF codes are turned into a 4-bit binary number by the 4 output pins of the 8870, and requires reading these 4 pins and turning that back into a decimal number. The python program just listens for the flag pin to go high from the 8870, alerting the program that the 8870 has decoded a new DTMF code. The pins are read to determine the binary code and is added to a list of lists. Once the final code is received (the '\*' key, 1011 in binary), each element in the list is converted back to a decimal number. Finally this is compared to the final if/elif series to determine what to do.

The last functions just produce different print statements as an example, but can be customized to fit whatever you need. Control a repeater, activate a relay to turn on the lights, open the garage door.
